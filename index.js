import express from 'express';
import dotenv from 'dotenv';
import morgan from 'morgan'
import fs from 'fs'

import { disableCors } from './middlewares.js'
import { createFile } from './controllers/createFile.js'
import { getFiles } from './controllers/getFiles.js'
import { getFileByFilename } from './controllers/getFileByFilename.js'
import { deleteFile } from './controllers/deleteFile.js';

dotenv.config();
const PORT = process.env.PORT ?? 8080;

let app = express();

app.use(disableCors);
app.use(express.json());
app.use(express.urlencoded({ extended: false }))
app.use(morgan('common', {
    stream: fs.createWriteStream('./logs.log', { flags: 'a' })
}));
app.use(morgan('dev'));


app.post('/api/files', createFile);
app.get('/api/files', getFiles);
app.get('/api/files/:filename', getFileByFilename);
app.delete('/api/files/:filename', deleteFile);

app.listen(PORT, () => {
    console.log("Server started!")
})