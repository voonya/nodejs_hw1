import fs from 'fs/promises'
import { getDestination } from '../functions/getDestination.js'
import path from 'path'

export function deleteFile(req, res) {
    const { filename } = req.params;
    if (filename) {
        const __destinationDir = getDestination();
        fs.rm(path.join(__destinationDir, filename))
            .then(() => {
                res.status(200).json({
                    message: 'Success'
                })
            })
            .catch((err) => {
                if (err.code === 'ENOENT') {
                    res.status(400).json({
                        message: 'No file with this name'
                    })
                    return;
                }
                res.status(500).json({
                    message: 'Server error'
                })
            })
        return;
    }
    return res.status(400).json({
        message: 'Set filename to delete it'
    })
}