import fs from 'fs/promises'
import path from 'path'
import { getDestination } from '../functions/getDestination.js'

export async function getFileByFilename(req, res) {
    const __destinationDir = getDestination();
    const { filename } = req.params;
    if (filename) {
        let fileData = {};
        fileData.filename = filename;

        await fs.readFile(path.join(__destinationDir, filename), 'utf8')
            .then((data) => {
                fileData.content = data;
            })
            .then(() => fs.stat(path.join(__destinationDir, filename)))
            .then(data => {
                fileData.uploadedDate = data.birthtime;
                fileData.extension = filename.slice(filename.lastIndexOf('.') + 1);
                res.status(200).json({
                    message: 'Success',
                    ...fileData
                })
            })
            .catch((err) => {
                if (err.code === 'ENOENT') {
                    return res.status(400).json({
                        message: `No file with '${filename}' filename found`
                    })
                }
                res.status(500).json({
                    message: 'Internal server error'
                })
            });
        return;
    }
    return res.status(400).json({
        message: `Please specify filename`
    })
}