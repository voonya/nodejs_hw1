import fs from 'fs'
import fsPromises from 'fs/promises'
import path from 'path'
import { getDestination } from '../functions/getDestination.js'

const writeFile = fsPromises.writeFile;


const regularExpFormats = /^[\w\._-]+\.(log|txt|json|yaml|xml|js)/;

export async function createFile(req, res) {
    const __destinationDir = getDestination();

    if (!fs.existsSync(__destinationDir)) {
        fs.mkdirSync(__destinationDir);
    }

    const filename = req.body.filename;
    const content = req.body.content;

    if (filename && content) {

        if (!regularExpFormats.test(filename)) {
            return res.status(400).json({
                message: 'Unsupported file extension'
            })
        }

        if (fs.existsSync(path.join(__destinationDir, filename))) {
            return res.status(400).json({
                message: 'File already exists'
            })
        }

        await writeFile(path.join(__destinationDir, filename), content)
            .then(() => {
                res.status(200).json({
                    message: 'File created successfully'
                });
            })
            .catch(() => {
                return res.status(500).json({
                    message: 'Server error'
                })
            })
        return;
    }
    return res.status(400).json({
        message: `Please specify ${filename ? 'content' : content ? 'filename' : 'filename, content'}`
    })
}