import fs from 'fs/promises'
import { getDestination } from '../functions/getDestination.js'



export function getFiles(req, res) {
    const __destinationDir = getDestination();
    fs.readdir(__destinationDir)
        .then((files) => {
            res.status(200).json({
                message: 'Success',
                files
            })
        })
        .catch((err) => {
            if (err.code === 'ENOENT') {
                return res.status(200).json({
                    message: 'Success',
                    files: []
                })
            }
            res.status(500).json({
                message: 'Internal server error'
            })
        })
}