import path from 'path';
import dotenv from 'dotenv'

dotenv.config();

export function getDestination() {
    return path.resolve(process.env.FOLDER_NAME);
}

